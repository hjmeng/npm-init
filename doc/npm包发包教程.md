# 如何自制npm包

## 为什么使用npm

1. npm 是Node 的模块管理器，功能极其强大。

2. 有了npm，我们只要一行命令，就能安装别人写好的模块。

## 创建第一个node模块 【Node.js模块就是发布到npm的代码包】

1. 第一步：用npm init来创建package.json文件。

2. package.json文件中模块的名字和版本号是必填项。

3. 需要有一个入口文件 如果没有的话默认为index.js

```js

exports.showMsg = function () {
  console.log("这是我的第一个包");
};

```

4. 需要使用 exports导出方法 （别人的代码里可以import或者require导入方法使用）

## 如何发布到npm服务器上

### 方法一：（打开npm 注册）

1. 去npm官网注册一个npm账号   https://www.npmjs.com/signup

2. 在创建的文件夹中npm login 登录 （首次需要登录,存储证书到本地,后面就不需要每次都登录了）

3. 切记要通过邮箱验证

### 方法二：使用 npm adduser （命令行注册，输入如下）

1. npm adduser

2. 依次输入用户名，密码，邮箱就注册成功了。注册成功会自动登录，所以现在已经在本地登录成功。

## 登录（如果是首次注册并且没有登陆过npm则自动登录）

```sh

npm set registry https://registry.npmjs.org
npm login
# 输入用户名，你注册的密码，邮箱号

```

## 开始发布

1. 使用命令行 npm publish 发布包

2. 发布过程会把整个目录发布,不想发布的内容模块,可以通过 .gitignore 或 .npmignore 文件忽略

3. 发布成功后 你的包名就是 package.json中的name了  如：我的包名就是 hjm-console

4. 发布成功之后可以去npm官网搜索一下是否已经存在

## 如何更新npm包

1. 使用命令行 npm version patch 更新package.json里面的版本号

1. npm version 1.0.2 [package.json修改为 1.0.2版本]

2. 使用 npm publish,更新就会完成

3. [ 注：patch会自动更新package.json里面的版本号 ]

## 测试使用

1. 创建一个文件夹，在文件夹下面 npm install 包名（我的是 hjm-console）

2. 然后你会发现文件夹下面有一个node_modules文件 打开后你会发现你的包会出现在文件夹下面

3. 新建一个index.js 使用你创建的包

```js
var hjm = require('hjm-console');
hjm.showMsg();

```
4. 只用命令行 node index 运行index.js 运行结果显示：这是我的第一个包

5. 如果上述测试正常显示则你已成功创建了属于自己的npm包

## 删除包

```sh

npm unpublish hjm-web-utils --force
# 注意:删除包后需要在24小时之后才能重新上传包名相同的包
# 半小时内你可以删除自己发布的库，之后你就再也不能删除了。
# 原因：删库事件导致很多依赖它的著名的npm包构建失败，甚至影响到了不少公司的生产环境。从那时候开始npm就更改了unpublish的策略。

```

## 发布过程中遇到的bug解决方法

1. you must verify your email before publishing a new package: https://www.npmjs.com/email-edit : hjm-console

1. 您没有验证您的电子邮件地址。在注册的时候给你发的邮件要验证

2. You cannot publish over the previously published version 1.0.1. : hjm-console

2. 你不能发布当前版本1.0.1（每次发布新版本的时候请修改package中的版本号 --> 版本号不能重复）

2. 可以手动修改也可以通过命令行修改：详细请查看## 如何更新npm包

3. 报错："hjm-vFilters" is invalid for new packages : hjm-vFilters 

3. 包名中不再允许使用大写字符。将包名改为hjm-vue-filters

