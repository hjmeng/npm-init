# 初始化的npm包架构
## 场景

1. 每次搭建新包架构的时候需要从新配置架构

## 设计和思路

1. 初始化一个纯净项目[项目不包含任何框架类结构]
1. 保持项目的纯净化
1. 编写一个脚手架用于快速搭建项目

## 项目架构

```sh
.
├── build                           # webpack打包脚本
│   ├── webpack.base.conf.js          # 打包规则配置
│   ├── webpack.build.conf.js         # axios分装打包umd
│   ├── webpack.prod.conf.js          # demo打包配置
│   └── webpack.dev.conf.js           # demo运行环境配置
├── demo                           # 开发demo演示
│   ├── index.ts
│   └── index.html
├── src                         # 源文件
│   └── index.ts                
├── doc                   # 技术文档    
├── plan                  # 开发计划 
├── tests                 # 测试文件
└── README.md             # 说明文件

```

## 安装教程

```shell

git clone https://gitee.com/hjmeng/package-name.git   # 克隆项目到本地
cd package-name                                       # 进入项目根目录
npm i                                                 # 安装包依赖

```
### 启动脚本

```shell
#------------- demo --------------#
npm run dev            # 启动开发环境

npm run build:demo     # 打包demo环境测试

#------------- build --------------#
npm run build          # 打包npm

#------------- lint --------------#
npm run lint         # 启动lint检测

npm run lint:fix     # 启动lint修复

```

### 发布包

```shell
#------------查看当前npm源的地址-------------#
npm config get registry
#------------登录npm(如果有则不用登陆)-------------#
npm set registry https://registry.npmjs.org

npm login

#------------发布包(注意package.json中的版本)-------------#
npm version patch # 更新package中的版本号 npm version 1.0.1
npm publish

```
## 用法

### api配置

```js
console.log('api配置')

```

### 包使用

```js
console.log('包使用')

```
#### 分支说明

1. master           默认分支

1. develop          调试分支

#### 参与贡献

1.  Fork 本仓库

2.  新建 Feat_xxx 分支

3.  提交代码

4.  新建 Pull Request