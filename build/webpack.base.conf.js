const path = require('path')
module.exports = {
  entry: path.resolve(__dirname, '../demo'),
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, '../demo/dist')
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: ['ts-loader']
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
  },
  resolve: {
    extensions: [".ts", ".js"]
  },
  plugins: []
}
