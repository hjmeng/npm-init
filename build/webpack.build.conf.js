const path = require('path')
module.exports = {
  entry: path.resolve(__dirname, '../src'),
  output: {
    path: path.resolve(__dirname, '../dist'),
    library: 'Request',
    libraryTarget: 'umd'
  },
  resolve: {
    extensions: [".ts", ".js"]
  },
  mode: 'production',
  module: {
    rules: [{
      test: /\.ts$/,
      include: path.join(__dirname, '../src/'),
      exclude: /node_modules/,
      use: ['babel-loader', 'ts-loader']
    }]
  }
}
