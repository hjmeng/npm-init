const path = require('path')
const baseWebpackConf = require('./webpack.base.conf')
const { merge } = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const prodWebpackConf = merge(baseWebpackConf, {
  mode: 'production',
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, '../demo/index.html'),
      inject: true,
      minify: { // 压缩html
        removeComments: true, // 删除注释
        collapseWhitespace: true, // 删除空格
        removeAttributeQuotes: true, // 是去掉属性的双引号
        minifyCSS: true // 删除样式中的空格
      }
    })
  ]
})
module.exports = prodWebpackConf

