const path = require('path')
const ip = require('ip')
const baseWebpackConf = require('./webpack.base.conf')
const { merge } = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const devWebpackConf = merge(baseWebpackConf, {
  devServer: {
    hot: true, // 热加载
    inline: true, // 自动刷新
    open: false, // 自动打开浏览器
    progress: true, // 将运行进度输出到控制台。
    historyApiFallback: true, // 在开发单页应用时非常有用，它依赖于HTML5 history API，如果设置为true，所有的跳转将指向index.html
    host: '0.0.0.0', // 主机名
    port: 8888, // 端口号
    proxy: {  // 配置反向代理解决跨域
      '/api': {
        target: 'http://hjm100.cn',
        ws: true,
        changeOrigin: true
      }
    },
    compress: true, // 为你的代码进行压缩。加快开发流程和优化的作用
    overlay: {
      // 在浏览器上全屏显示编译的errors或warnings。
      errors: true,
      warnings: false
    },
    quiet: true // 终端输出的只有初始启动信息。 webpack 的警告和错误是不输出到终端的
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, '../demo/index.html'),
      inject: true,
    }),
    new FriendlyErrorsPlugin({
      compilationSuccessInfo: {
        messages: [
          `App running at:\n
          - Local:   http://localhost:8888/
          - Network: http://${ip.address()}:8888/`
        ]
      }
    })
  ]
})
module.exports = devWebpackConf;
